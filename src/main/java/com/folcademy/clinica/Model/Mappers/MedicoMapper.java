package com.folcademy.clinica.Model.Mappers;
import com.folcademy.clinica.Model.Dtos.MedicoDtoMostrar;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;


    public MedicoMapper(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public MedicoDto entityToDto(Medico entity){
        return Optional /*objeto optional que puede tomar dos caminos  */
                .ofNullable(entity)/* si es null salta al orElse */
                .map( ent -> new MedicoDto( //ent es medico
                        ent.getIdmedico(),
                        null,
                        null,
                        ent.getProfesion(),
                        null,
                        null,
                        ent.getConsulta(),
                        ent.getIdpersona(),
                        ent.getPersona())
                )
                .orElse(new MedicoDto());
    }
    public MedicoDtoMostrar entityToDtoMostrar(Medico entity){
        return Optional /*objeto optional que puede tomar dos caminos  */
                .ofNullable(entity)/* si es null salta al orElse */
                .map( ent -> new MedicoDtoMostrar( //ent es medico
                        ent.getIdmedico(),
                        ent.getProfesion(),
                        ent.getConsulta(),
                        ent.getIdpersona(),
                        ent.getPersona())
                )
                .orElse(new MedicoDtoMostrar());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        PersonaDto personaDto = new PersonaDto();
        personaDto.setIdpersona(dto.getIdpersona());
        personaDto.setApellido(dto.getApellido());
        personaDto.setNombre(dto.getNombre());
        personaDto.setDni(dto.getDni());
        personaDto.setTelefono(dto.getTelefono());
        Persona p1 = personaRepository.save(personaMapper.dtoToEntity(personaDto));
        entity.setIdmedico(dto.getIdmedico());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(p1.getIdpersona());
        entity.setPersona(dto.getPersona());

        return entity;
    }
    public Medico dtoMostrarToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setIdmedico(dto.getIdmedico());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(dto.getIdpersona());
        return entity;
    }
}
