package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteDtoMostrar;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PacienteMapper(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map( ent -> new PacienteDto(
                        ent.getIdpaciente(),
                        null,
                        null,
                        null,
                        ent.getDireccion(),
                        null,
                        ent.getIdpersona(),
                        ent.getPersona()))
                .orElse(new PacienteDto());

    }


    public PacienteDtoMostrar entityToDtoMostrar(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map( ent -> new PacienteDtoMostrar(
                        ent.getIdpaciente(),
                        ent.getDireccion(),
                        ent.getIdpersona(),
                        ent.getPersona())
                )
                .orElse(new PacienteDtoMostrar());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        PersonaDto personaDto = new PersonaDto();
        personaDto.setIdpersona(dto.getIdpersona());
        personaDto.setApellido(dto.getApellido());
        personaDto.setNombre(dto.getNombre());
        personaDto.setDni(dto.getDni());
        personaDto.setTelefono(dto.getTelefono());
        Persona p1= personaRepository.save(personaMapper.dtoToEntity(personaDto));
        entity.setIdpaciente(dto.getId());
        entity.setDireccion(dto.getDireccion());
        entity.setIdpersona(p1.getIdpersona());
        entity.setPersona(dto.getPersona());

        return entity;
    }
    public Paciente dtoMostrarToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setIdpaciente(dto.getId());
        entity.setDireccion(dto.getDireccion());
        entity.setIdpersona(dto.getIdpersona());
        entity.setPersona(dto.getPersona());
        return entity;
    }

}
