package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "medico")
@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor

public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition  = "INT(10) UNSIGNED")
    public Integer idmedico;
    public String profesion="";
    @Column(name = "consulta")
    public Integer consulta;


    @Column(name = "idpersona", columnDefinition = "INT")
    public Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false, updatable = false)
    private Persona persona;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;

        return Objects.equals(idmedico, medico.idmedico);
    }

    @Override
    public int hashCode() {
        return 47971316;
    }
}



