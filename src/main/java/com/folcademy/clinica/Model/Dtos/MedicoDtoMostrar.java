package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDtoMostrar {
    @NotNull
    Integer idmedico;
    @NotNull
    String profesion;
    @NotNull
    int consulta;

    public Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false, updatable = false)
    private Persona persona;

}
