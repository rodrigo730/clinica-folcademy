package com.folcademy.clinica.Model.Dtos;


import com.folcademy.clinica.Model.Entities.Persona;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class MedicoDto {
    @NotNull
    Integer idmedico;
    String nombre;
    String apellido;
    @NotNull
    String profesion;
    String dni;
    String telefono;
    @NotNull
    int consulta;

    public Integer idpersona;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false, updatable = false)
    private Persona persona;

}
