package com.folcademy.clinica.Model.Dtos;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    @NotNull
    Integer idturno;
    @NotNull
    LocalDate fecha;
    @NotNull
    LocalTime hora;
    @NotNull
    Boolean atendido;
    @NotNull
    Integer idpaciente;
    @NotNull
    Integer idmedico;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name ="idpaciente", referencedColumnName = "idpaciente",insertable = false,updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name ="idmedico", referencedColumnName = "idmedico", insertable = false, updatable = false)
    private Medico medico;
}
