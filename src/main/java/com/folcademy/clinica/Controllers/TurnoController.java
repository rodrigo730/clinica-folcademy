package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/turno")
public class TurnoController {
    private final TurnoService turnoService;
    private TurnoController(TurnoService turnoService){
        this.turnoService = turnoService;
    }

    //@PreAuthorize("hasAuthority('get_turnos')")
    @GetMapping("")
    public ResponseEntity<Page<TurnoDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idturno") String orderField){

        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("/{id}")
    public ResponseEntity<Page<TurnoDto>> listarByPage(
            @PathVariable(name = "id") Integer id,
            @RequestParam(name = "pageNumber",defaultValue ="0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idturno") String orderField){

        return ResponseEntity.ok(turnoService.listarByPage(id,pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("")
    public  ResponseEntity<TurnoDto> agregar(@RequestBody TurnoDto entity){
        return ResponseEntity.ok(turnoService.agregar(entity));
    }


    //@PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("/{idturno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idturno") int id,
                                              @RequestBody TurnoDto dto) {
        return ResponseEntity.ok(turnoService.editar(id, dto));
    }

    //@PreAuthorize("hasAuthority('del_turno')")
    @DeleteMapping("/{idturno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idturno") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }

}
