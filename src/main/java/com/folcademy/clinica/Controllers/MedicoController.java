package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoDtoMostrar;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/medico")
public class MedicoController {
    private final MedicoService medicoService;

    private MedicoController(MedicoService medicoService) {

        this.medicoService = medicoService;
    }

    //@PreAuthorize("hasAuthority('get_medicos')")
    @GetMapping("")
    public ResponseEntity<Page<MedicoDtoMostrar>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idmedico") String orderField){

        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('get_medico')")
    @GetMapping("/{idmedico}")
    public ResponseEntity<Page<MedicoDtoMostrar>> listarByPage(
            @PathVariable(name = "idmedico") Integer id,
            @RequestParam(name = "pageNumber",defaultValue ="0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idmedico") String orderField){

        return ResponseEntity.ok(medicoService.listarByPage(id,pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('post_medico')")
    @PostMapping("")
    public  ResponseEntity<MedicoDtoMostrar> agregar(@RequestBody @Validated MedicoDto dto){
        return ResponseEntity.ok(medicoService.agregar(dto));
    }
    //@PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoDtoMostrar> editar(@PathVariable (name = "idMedico") int id,
                                                  @RequestBody @Validated MedicoDto dto) {
        return ResponseEntity.ok(medicoService.editar(id, dto));
    }
    //@PreAuthorize("hasAuthority('del_medico')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }

}

