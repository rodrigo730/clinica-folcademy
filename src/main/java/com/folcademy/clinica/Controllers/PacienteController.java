package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteDtoMostrar;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    private PacienteController(PacienteService pacienteService) {

        this.pacienteService = pacienteService;
    }
    //@PreAuthorize("hasAuthority('get_pacientes')")
    @GetMapping("")
    public ResponseEntity<Page<PacienteDtoMostrar>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idpaciente") String orderField){

        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping("/{idpaciente}")
    public ResponseEntity<Page<PacienteDtoMostrar>> listarByPage(
            @PathVariable(name = "idpaciente") Integer id,
            @RequestParam(name = "pageNumber",defaultValue ="0") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "idpaciente") String orderField){

        return ResponseEntity.ok(pacienteService.listarByPage(id,pageNumber,pageSize,orderField));
    }
    //@PreAuthorize("hasAuthority('post_paciente')")
    @PostMapping("")
    public  ResponseEntity<PacienteDtoMostrar> agregar(@RequestBody @Validated PacienteDto dto){
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }
    //@PreAuthorize("hasAuthority('put_paciente')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDtoMostrar> editar(@PathVariable(name = "idPaciente") int id,
                                            @RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.editar(id, dto));
    }
    //@PreAuthorize("hasAuthority('del_paciente')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
