package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptins.NotFoundException;
import com.folcademy.clinica.Exceptins.ValidationException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service("turnoService")

public class TurnoService {
    private final MedicoRepository medicoRepository;
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper, MedicoRepository medicoRepository) {
        this.medicoRepository = medicoRepository;
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    public Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return  turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    public Page<TurnoDto> listarByPage(Integer idturno,Integer pageNumber, Integer pageSize,String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return turnoRepository.findById(idturno, pageable).map(turnoMapper::entityToDto);
    }

    public TurnoDto agregar(TurnoDto entity) {
        if (entity.getFecha()!=null) {
            entity.setIdturno(null);
            return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
        }
        throw new ValidationException("La fecha no puede ser nula");

    }
    public TurnoDto editar(Integer idturno, TurnoDto dto) {
        if (!turnoRepository.existsById(idturno)){
            throw new NotFoundException("No se encontro el id del turno o no existe");}
        if (!medicoRepository.existsById(dto.getIdmedico()))
            throw new NotFoundException("No se encontro el id del medico o no existe");
        dto.setIdturno(idturno);
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }
    public boolean eliminar(Integer id) {
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("No se encontro el id o no existe");
        turnoRepository.deleteById(id);
        return true;
    }
}
