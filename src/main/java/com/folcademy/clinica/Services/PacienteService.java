package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptins.NotFoundException;
import com.folcademy.clinica.Exceptins.ValidationException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteDtoMostrar;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


@Service("pacienteService")
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {

        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public Page<PacienteDtoMostrar> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return  pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDtoMostrar);
    }

    public Page<PacienteDtoMostrar> listarByPage(Integer idpaciente,Integer pageNumber, Integer pageSize,String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
        return pacienteRepository.findById(idpaciente, pageable).map(pacienteMapper::entityToDtoMostrar);
    }
        public  PacienteDtoMostrar agregar(PacienteDto dto){
        if (dto.getDni().isEmpty())
            throw new ValidationException("Se ingreso un dato vacio");
        dto.setId(null);
        return pacienteMapper.entityToDtoMostrar(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));
    }

    public PacienteDtoMostrar editar(Integer idPaciente, PacienteDto dto) {
        if (!pacienteRepository.existsById(idPaciente)){
            throw new NotFoundException("No se encontro el id o no existe");}
        if (!dto.getTelefono().matches("[0-9]*")){
            throw new ValidationException("El numero de telefono no es un numero");}
        dto.setId(idPaciente);
        Integer indice = pacienteRepository.findById(idPaciente).map(pacienteMapper::entityToDto).orElse(null).getIdpersona();//guardo id para borrar persona
        pacienteMapper.entityToDtoMostrar(pacienteRepository.save(pacienteMapper.dtoToEntity(dto)));//edicion de medico
        personaRepository.deleteById(indice); //borro persona
        return pacienteRepository.findById(idPaciente).map(pacienteMapper::entityToDtoMostrar).orElse(null); //muestro medico

    }
    public boolean eliminar(Integer id) {
        if (!pacienteRepository.existsById(id))
            throw new NotFoundException("No se encontro el id o no existe");
        Integer indice = pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null).getIdpersona();//guardo id para borrar persona
        pacienteRepository.deleteById(id);
        personaRepository.deleteById(indice);
        return true;
    }

}
