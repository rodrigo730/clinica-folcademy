package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptins.BadRequestException;
import com.folcademy.clinica.Exceptins.NotFoundException;
import com.folcademy.clinica.Exceptins.ValidationException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoDtoMostrar;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service("medicoService")
public class MedicoService { /*inicializacion*/
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;


    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.medicoRepository = medicoRepository; /* constructores */
        this.medicoMapper = medicoMapper;

        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }


    public Page<MedicoDtoMostrar> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return  medicoRepository.findAll(pageable).map(medicoMapper::entityToDtoMostrar);
    }

    public Page<MedicoDtoMostrar> listarByPage(Integer idmedico,Integer pageNumber, Integer pageSize,String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return  medicoRepository.findById(idmedico,pageable).map(medicoMapper::entityToDtoMostrar);
    }
    public MedicoDtoMostrar agregar(MedicoDto dto){
        if (!dto.getProfesion().isEmpty()) {
            dto.setIdmedico(null);
            return medicoMapper.entityToDtoMostrar(medicoRepository.save(medicoMapper.dtoToEntity(dto)));
        }
        //System.out.println("true");
        throw new ValidationException("Se ingreso un dato vacio");

    }
    public MedicoDtoMostrar editar(Integer idMedico, MedicoDto dto) {
        if (!medicoRepository.existsById(idMedico))
            throw new NotFoundException("No se encontro el id o no existe");
        if (dto.getConsulta()<200)
            throw new BadRequestException("Error la consulta no puede ser menor a 200");
        dto.setIdmedico(idMedico);
        Integer indice = medicoRepository.findById(idMedico).map(medicoMapper::entityToDto).orElse(null).getIdpersona(); //guardo id para borrar persona
        medicoMapper.entityToDtoMostrar(medicoRepository.save(medicoMapper.dtoToEntity(dto)));//edicion de medico
        personaRepository.deleteById(indice); //borro persona
        return medicoRepository.findById(idMedico).map(medicoMapper::entityToDtoMostrar).orElse(null); //muestro medico

    }

    public boolean eliminar(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("No se encontro el id o no existe");
        Integer indice = medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null).getIdpersona();//guardo id para borrar persona
        medicoRepository.deleteById(id);
        personaRepository.deleteById(indice);
        return true;
    }


}
