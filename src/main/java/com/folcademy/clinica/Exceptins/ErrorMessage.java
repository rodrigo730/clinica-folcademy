package com.folcademy.clinica.Exceptins;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
    private String message;
    private String detail;
    private String code;
    private String path;


}
