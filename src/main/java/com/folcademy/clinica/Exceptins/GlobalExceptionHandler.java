package com.folcademy.clinica.Exceptins;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> NotFoundExceptionHandler(HttpServletRequest req, Exception exc){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error en id",exc.getMessage(),"1",req.getRequestURI()), HttpStatus.NOT_EXTENDED);
    }
    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> ValidationExceptionHandler(HttpServletRequest req, Exception exc) {
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error al ingresar dato", exc.getMessage(), "2", req.getRequestURI()), HttpStatus.NOT_FOUND);

    }
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    public ResponseEntity<ErrorMessage> BadRequestExceptionHandler(HttpServletRequest req, Exception exc){
        return new ResponseEntity<ErrorMessage>(new ErrorMessage("Error artimetico",exc.getMessage(),"3",req.getRequestURI()), HttpStatus.NOT_FOUND);
    }
}
