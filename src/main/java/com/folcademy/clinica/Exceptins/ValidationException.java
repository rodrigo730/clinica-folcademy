package com.folcademy.clinica.Exceptins;

public class ValidationException extends RuntimeException{
    public ValidationException(String message){
        super(message);
    }
}
