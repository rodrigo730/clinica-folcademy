package com.folcademy.clinica.Exceptins;

public class BadRequestException extends RuntimeException{
    public BadRequestException(String message){
        super(message);
    }
}
